<?php

use IpelaMarshal\Contracts\IProcessDefinition;
use IpelaMarshal\Contracts\IState;
use IpelaMarshal\Helpers\ValidStateChecker;
use Tests\TestCase;

class ValidStateCheckerTest extends TestCase
{
    public function test_can_check_valid_state()
    {
        $this->assertFalse(ValidStateChecker::check("Boo"));
        $this->assertTrue(ValidStateChecker::check(ValidState::class));
        $this->assertFalse(ValidStateChecker::check(SomeClass::class));
    }
}

class ValidState implements IState
{

}

class SomeClass
{
}