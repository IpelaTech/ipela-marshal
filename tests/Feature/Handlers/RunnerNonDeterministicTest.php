<?php


use IpelaMarshal\Contracts\IProcessDefinition;
use IpelaMarshal\Contracts\IState;
use IpelaMarshal\Data\RunnerArgs;
use IpelaMarshal\Enums\DefaultTransitionEnum;
use IpelaMarshal\Exceptions\MarshalException;
use IpelaMarshal\Handlers\RunnerDeterministic;
use IpelaMarshal\Handlers\RunnerNonDeterministic;
use Tests\TestCase;

class RunnerNonDeterministicTest  extends TestCase
{
    public function setUp() : void
    {
        parent::setUp();
    }

    public function test_can_run_non_deterministic_process_path1()
    {
        $args = new RunnerArgs;
        $args->process_definition = new NonDeterministic;
        $args->current_state = TestNonDeterministicStateOne::class;
        $args->transition = "yes";

        $res1 = RunnerNonDeterministic::run($args);
        
        $this->assertEquals(TestNonDeterministicStateTwo::class, $res1);

        $args->current_state = TestNonDeterministicStateTwo::class;
        $args->transition = "up";

        $res2 = RunnerNonDeterministic::run($args);

        $this->assertEquals(TestNonDeterministicStateThree::class, $res2);

        $args->current_state = TestNonDeterministicStateThree::class;
        $args->transition = "left";

        $res3 = RunnerNonDeterministic::run($args);

        $this->assertEquals(TestNonDeterministicStateFour::class, $res3);

        $args->current_state = TestNonDeterministicStateFour::class;
        $args->transition = "again";

        $res4 = RunnerNonDeterministic::run($args);

        $this->assertFalse($res4);
    }

    public function test_can_run_non_deterministic_process_path2()
    {
        $args = new RunnerArgs;
        $args->process_definition = new NonDeterministic;
        $args->current_state = TestNonDeterministicStateOne::class;
        $args->transition = "no";
        
        $res1 = RunnerNonDeterministic::run($args);

        $this->assertEquals(TestNonDeterministicStateThree::class, $res1);

        $args->current_state = TestNonDeterministicStateThree::class;
        $args->transition = "right";

        $res2 = RunnerNonDeterministic::run($args);

        $this->assertEquals(TestNonDeterministicStateTwo::class, $res2);

        $args->current_state = TestNonDeterministicStateTwo::class;
        $args->transition = "down";

        $res3 = RunnerNonDeterministic::run($args);

        $this->assertEquals(TestNonDeterministicStateFour::class, $res3);

        $args->current_state = TestNonDeterministicStateFour::class;
        $args->transition = "down";

        $res4 = RunnerNonDeterministic::run($args);
        
        $this->assertFalse($res4);
    }

    public function test_can_reject_wrong_state()
    {
        $this->expectException(MarshalException::class);
        $args = new RunnerArgs;
        $args->process_definition = new NonDeterministic;
        $args->current_state = "bad info";
        $args->transition = "no";

        $res1 = RunnerNonDeterministic::run($args);
    }
}

class NonDeterministic implements IProcessDefinition
{
    public function get_definition($args = null) : array
    {
        return [
            TestNonDeterministicStateOne::class => [
                "yes" => TestNonDeterministicStateTwo::class,
                "no" => TestNonDeterministicStateThree::class
            ],
            TestNonDeterministicStateTwo::class  => [
                "up" => TestNonDeterministicStateThree::class,
                "down" => TestNonDeterministicStateFour::class
            ],
            TestNonDeterministicStateThree::class  => [
                "left" => TestNonDeterministicStateFour::class,
                "right" => TestNonDeterministicStateTwo::class
            ],
            TestNonDeterministicStateFour::class  => [
                "again" => TestNonDeterministicStateOne::class
            ]
        ];
    }
}

class TestNonDeterministicStateOne implements IState
{
    public function on_yes()
    {
    }

    public function on_no()
    {
    }
}

class TestNonDeterministicStateTwo implements IState
{
    public function on_up()
    {
    }

    public function on_down()
    {
    }
}

class TestNonDeterministicStateThree implements IState
{
    public function on_left()
    {
    }

    public function on_right()
    {
    }
}

class TestNonDeterministicStateFour implements IState
{
    public function on_again()
    {
    }
}
