<?php

use IpelaMarshal\Contracts\IProcessDefinition;
use IpelaMarshal\Contracts\IState;
use IpelaMarshal\Data\RunnerArgs;
use IpelaMarshal\Enums\DefaultTransitionEnum;
use IpelaMarshal\Exceptions\MarshalException;
use IpelaMarshal\Handlers\Runner;
use Tests\TestCase;

class RunnerTest extends TestCase
{
    //as long as there are no errors thrown, these tests are fine
    public function test_can_run_deterministic()
    {
        $this->expectNotToPerformAssertions();

        $definition = new TestDeterministicDefinition;
        $args = new RunnerArgs;
        $args->process_definition = $definition;
        $args->current_state = TestState::class;
        $runner = Runner::run($args);
    }

    public function test_can_run_non_deterministic() 
    {
        $this->expectNotToPerformAssertions();

        $definition = new TestNonDeterministicDefinition;
        $args = new RunnerArgs;
        $args->process_definition = $definition;
        $args->current_state = TestState::class;
        $runner = Runner::run($args);
    }

    public function test_can_reject_bad_state()
    {
        $this->expectException(MarshalException::class);
        $args = new RunnerArgs;
        $args->current_state = "bad info";
        $runner = Runner::run($args);
    }
}

class TestState implements IState
{
    public function on_execute()
    {
    }

    public function on_yes()
    {
    }

    public function on_no()
    {
    }
}

class TestDeterministicDefinition implements IProcessDefinition
{
    public function get_definition($args = null) : array
    {
        return [
            TestState::class,
            TestState::class,
            TestState::class,
            TestState::class
        ];
    }
}

class TestNonDeterministicDefinition implements IProcessDefinition
{
    public function get_definition($args = null) : array
    {
        return [
            TestState::class => ["yes" => TestState::class, "no" => TestState::class],
            TestState::class => ["yes" => TestState::class, "no" => TestState::class],
            TestState::class => ["yes" => TestState::class, "no" => TestState::class],
            TestState::class => ["yes" => TestState::class, "no" => TestState::class],
        ];
    }
}