<?php


use IpelaMarshal\Contracts\IProcessDefinition;
use IpelaMarshal\Contracts\IState;
use IpelaMarshal\Data\RunnerArgs;
use IpelaMarshal\Enums\DefaultTransitionEnum;
use IpelaMarshal\Exceptions\MarshalException;
use IpelaMarshal\Handlers\RunnerDeterministic;
use Tests\TestCase;

class RunnerDeterministicTest extends TestCase
{
    public function setUp() : void
    {
        parent::setUp();
    }

    public function test_can_run_deterministic_process()
    {
        $args = new RunnerArgs;
        $args->process_definition = new Deterministic;
        $args->current_state = TestDeterministicStateOne::class;
        
        $res1 = RunnerDeterministic::run($args);
        $this->assertEquals(TestDeterministicStateTwo::class, $res1);

        $args->current_state = TestDeterministicStateTwo::class;
        $res2 = RunnerDeterministic::run($args);
        $this->assertEquals(TestDeterministicStateThree::class, $res2);

        $args->current_state = TestDeterministicStateThree::class;
        $res3 = RunnerDeterministic::run($args);
        $this->assertFalse($res3);
    }

    public function test_pass_in_wrong_state()
    {
        $this->expectException(MarshalException::class);
        $args = new RunnerArgs;
        $args->process_definition = new Deterministic;
        $args->current_state = TestStep::class;
        
        $res1 = RunnerDeterministic::run($args);
    }
}

class Deterministic implements IProcessDefinition
{
    public function get_definition($args = null) : array
    {
        return [
            TestDeterministicStateOne::class,
            TestDeterministicStateTwo::class,
            TestDeterministicStateThree::class
        ];
    }
}
class TestDeterministicStateOne implements IState
{
    public function on_execute()
    {
        return true;
    }   
}
class TestDeterministicStateTwo implements IState
{
    public function on_execute()
    {
        return true;
    }  
}
class TestDeterministicStateThree implements IState
{
    public function on_execute()
    {
        return true;
    }  
}
class TestStep implements IState
{
    public function on_execute()
    {
        return true;
    }  
}
