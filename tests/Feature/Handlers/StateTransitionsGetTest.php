<?php

use IpelaMarshal\Contracts\IProcessDefinition;
use IpelaMarshal\Contracts\IState;
use IpelaMarshal\Enums\DefaultTransitionEnum;
use IpelaMarshal\Helpers\StateTransitionsGetter;
use Tests\TestCase;

class StateTransitionsGetTest extends TestCase
{

    public function test_can_get_deterministic()
    {
        $res = StateTransitionsGetter::get(
            new ProcessDefinitionDeterministic
        );

        $this->assertEquals("execute", $res);
    }

    public function test_can_get_non_deterministic() 
    {
        $res = StateTransitionsGetter::get(
            new ProcessDefinitionNonDeterministic,
            TestState1::class
        );

        $this->assertEquals(
            [
                "yes",
                "no"
            ], $res
        );
    }
}

class TestState1 implements IState
{
}

class TestState2 implements IState
{
}

class TestState3 implements IState
{
}

class TestState4 implements IState
{
}

class TestState5 implements IState
{
}

class ProcessDefinitionNonDeterministic implements IProcessDefinition
{
    public function get_definition($args = null) : array
    {
        return [
            TestState1::class => [
                "yes" => TestState3::class,
                "no" => TestState2::class
            ],
            TestState2::class => [
                "one" => TestState4::class,
                "two" => TestState5::class,
            ]
        ];
    }
}

class ProcessDefinitionDeterministic implements IProcessDefinition
{
    public function get_definition($args = null) : array
    {
        return [
            TestState1::class,
            TestState2::class,
        ];
    }
}