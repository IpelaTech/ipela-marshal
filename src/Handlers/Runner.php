<?php
namespace IpelaMarshal\Handlers;

use IpelaMarshal\Contracts\IProcessDefinition;
use IpelaMarshal\Data\RunnerArgs;
use IpelaMarshal\Enums\DefaultTransitionEnum;
use IpelaMarshal\Exceptions\MarshalException;
use IpelaMarshal\Handlers\RunnerDeterministic;
use IpelaMarshal\Handlers\RunnerNonDeterministic;
use IpelaMarshal\Helpers\AssociativeArrayChecker;
use IpelaMarshal\Helpers\ValidStateChecker;

class Runner
{
    public static function run(RunnerArgs $args)
    {
        if (!ValidStateChecker::check($args->current_state)) {
            throw new MarshalException("{$args->current_state} does not implement IState");
        }
        
        $definition = $args->process_definition->get_definition();

        if (AssociativeArrayChecker::check($definition)) {
            return RunnerNonDeterministic::run($args);
        }
        
        return RunnerDeterministic::run($args);
    }
}