<?php
namespace IpelaMarshal\Handlers;

use IpelaMarshal\Contracts\IProcessDefinition;
use IpelaMarshal\Data\RunnerArgs;
use IpelaMarshal\Exceptions\MarshalException;
use IpelaMarshal\Helpers\ValidStateChecker;

class RunnerNonDeterministic
{
    public static function run(RunnerArgs $args)
    {
        if (!ValidStateChecker::check($args->current_state)) {
            throw new MarshalException("{$args->current_state} does not implement IState");
        }

        $process_definition = $args->process_definition;
        $current_state = $args->current_state;
        $transition = $args->transition;
        $value = $args->value;
        
        $definition = $process_definition->get_definition();
        $definition_keys = array_keys($definition);
        $first_key = \array_key_first($definition_keys);
        $first_step = $definition_keys[$first_key];
        $current_key = \array_search($current_state, $definition_keys);

        if ($current_key === false) {
            $process_definition_class = get_class($process_definition);
            throw new MarshalException(
                "Process Definition {$process_definition_class} does not have a step {$current_state}"
            );
        }

        if ($current_key !== $first_key) {
            $steps_to_keep = [];

            for ($i = count($definition)-1; $i > -1; $i--) {
                $steps_to_keep[$i] = $definition_keys[$i];

                if ($i === $current_key) {
                    break;
                }
            }

            $definition_keys = \array_reverse($steps_to_keep);
        }

        $state = new $current_state;

        if (\method_exists($state, $transition)) {
            $res = \call_user_func_array([$state, "on_{$transition}"], [$value]);    
        }

        \array_shift($definition_keys);

        if (count($definition_keys) > 0) {
            $step = $definition[$current_state];
            return $step[$transition];
        }

        return false;
    }
}