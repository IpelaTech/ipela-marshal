<?php
namespace IpelaMarshal\Data;

use IpelaMarshal\Enums\DefaultTransitionEnum;

class RunnerArgs
{
    public $process_definition;
    public $current_state;
    public $transition = "execute";
    public $value = null;
}