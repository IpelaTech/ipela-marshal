<?php 
namespace IpelaMarshal\Contracts;


interface IProcessDefinition
{
    function get_definition($args = null) : array;
}