<?php
namespace IpelaMarshal\Helpers;

use IpelaMarshal\Contracts\IState;


class ValidStateChecker
{
    public static function check(string $class) : bool
    {
        if (!\class_exists($class)) { 
            return false;
        }

        $obj = new $class;

        return $obj instanceof IState;
    }
}