<?php
namespace IpelaMarshal\Helpers;

use IpelaMarshal\Contracts\IProcessDefinition;
use IpelaMarshal\Data\RunnerArgs;
use IpelaMarshal\Enums\DefaultTransitionEnum;


class StateTransitionsGetter
{
    public static function get(
        IProcessDefinition $process_definition,
        string $current_state = null
    ) {
        $definition = $process_definition->get_definition();

        if (AssociativeArrayChecker::check($definition)) {
            if ($current_state === null) {
                throw new MarshalException("Please provide a state to get keys from");
            }

            $value = $definition[$current_state];
            return \array_keys($value);
        }

        $res = new RunnerArgs;
        return $res->transition;
    }
}