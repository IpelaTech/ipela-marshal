<?php
namespace IpelaMarshal\Helpers;


class AssociativeArrayChecker
{
    //https://stackoverflow.com/a/652760/1581495
    //https://github.com/laravel/framework/blob/8.x/src/Illuminate/Collections/Arr.php#L389
    public static function check(array $array) : bool
    {
        $keys = array_keys($array);
        return array_keys($keys) !== $keys;
    }
}