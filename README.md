
# Marshal

A simple State Machine library for Laravel (tested on Laravel 8) but can also be used outside of Laravel, as none of the classes use anything Laravel-specific.

## Features

Ability to create: 
- Deterministic, and
- Non-Deterministic State Machines

### Deterministic

Deterministic machines in this context, are those defined with an array with integer keys (i.e. a regular array) and the values are the States. States are executed one after the other and there's no branching. There is one transition, a default method called execute, which is configurable. What this means is that in your State class, you define a method called *on_execute* for the Runner to call.

### Non-Deterministic

Non-Deterministic machines are defined by an associative array, where the State is the key and the transitions and the new states they return are defined as an array. You don't define the branching logic inside the State Machine itself, you just pass in the current state and desired transition.


## Installation

Install with composer

```bash
  composer require ipelatech/ipela-marshal
```

## Concepts
### State Machine
#### Runner

The class that runs the State Machine defined in the Process Definition. Internally, it checks whether the Process Definition is an associative array, then picks the correct specific Runner between the two below.

- Methods:
    - public static function run(RunnerArgs $args)

#### RunnerDeterministic

The Runner that runs Deterministic State Machines.

- Methods:
    - public static function run(RunnerArgs $args)
  
#### RunnerNonDeterministic

The Runner that runs NonDeterministic State Machines.

- Methods:
    - public static function run(RunnerArgs $args)
  
#### Process Definition

A class that implements IProcessDefinition and contains your Process Definition. There's only one method to implement, *get_definition* which returns an array.

#### State

A class that implements IState and contains all the transition methods. Deterministic States will have one method - by default, *execute*, Non-Deterministic will have as many as you want.
  
### Convenience Classes

#### Associative Array Checker

Checks whether an array is associative or not.
- Methods:
    - check(array $array) : bool

#### StateTransitionGetter

Gets the valid transitions from a State in a Non-Deterministic State Machine.
- Methods
    - get(IProcessDefinition $process_definition,
 string $current_state = null) : string or array

*Note: for Deterministic State Machines, you do not pass a current state.*
#### ValidStateChecker

Checks that the full qualified class name you pass in implements IState.
- Methods: 
    - check(string $class) : bool

## Usage/Examples

The generalised workflow for working with State Machines is:

1. Create a Process Definition that implements IProcessDefinition. Define your machine in *get_definition* as an array and return it
2. Create State/s that implement IState. Each state will contain the Transition Name/s you wish to use.
3. Call Runner::run() and pass in the arguments. The returned value will be the next state class, or false if there are no more states.

You can also use the different runners separately if you know ahead of time what type of State Machine you're running. Use *RunnerDeterministic* for Deterministic State Machines, use *RunnerNonDeterministic* for Non-Deterministic State Machines.

### Deterministic State Machine

Deterministic State Machines are those that have states that execute sequentially (i.e. never branch). As a result, we can get away with using a single transition name. 

To create a Deterministic State Machine, begin by creating a Process Definition (list the states in order of execution from first to last), implement the interface method *get_definition()*:

```php
use IpelaMarshal\Contracts\IProcessDefinition;

class Deterministic implements IProcessDefinition
{
    public function get_definition()
    {
        return [
            TestDeterministicStateOne::class,
            TestDeterministicStateTwo::class,
            TestDeterministicStateThree::class
        ];
    }
}
```

Create your state classes. They will subclass the IState abstract class. By default, for a Deterministic machine, the transition name is *execute*. The Runner will call *on_execute*, so make sure you have a public *on_<default_method>* method in your State class.  You can change the name of the default method in the config file. See here.

```php
use IpelaMarshal\Contracts\IState;

class TestDeterministicStateOne implements IState
{
    public function on_execute(){
        //do your work here
    }   
}

class TestDeterministicStateTwo implements IState
{
    public function on_execute(){
        //do your work here
    }
}

class TestDeterministicStateThree implements IState
{
    public function on_execute(){
        //do your work here
    }
}
```

Next, call the run method of the Runner class. 

```php
use IpelaMarshal\Handlers\Runner;

$process_definition = new Deterministic;
$current_state = new TestDeterministicStateOne;
$action = DefaultTransitionEnum::TRANSITION();
$value = "foo";

$res = Runner::run(
    $process_definition,
    $current_state,
    $action,
    $value
);

echo $res; //prints 'TestDeterministicStateTwo'
```

When there are no more states to run, the Runner will return false.

### Non-Deterministic State Machine

Non-Deterministic State Machines are those that have branching logic, that is, a State can have two or more transitions. 

To create a Non-Deterministic State Machine, begin by creating a Process Definition (list the states in order of execution from first to last):

```php
class NonDeterministic implements IProcessDefinition
{
    public function get_definition()
    {
        return [
            TestNonDeterministicStateOne::class => [
                "yes" => TestNonDeterministicStateTwo::class,
                "no" => TestNonDeterministicStateThree::class
            ],
            TestNonDeterministicStateTwo::class  => [
                "up" => TestNonDeterministicStateThree::class,
                "down" => TestNonDeterministicStateFour::class
            ],
            TestNonDeterministicStateThree::class  => [
                "left" => TestNonDeterministicStateFour::class,
                "right" => TestNonDeterministicStateTwo::class
            ],
            TestNonDeterministicStateFour::class  => [
                "again" => TestNonDeterministicStateOne::class
            ]
        ];
    }
}
```

States for this Definition will look like this:

```php
class TestNonDeterministicStateOne implements IState
{
    public function on_yes(){}
    public function on_no(){}
}

class TestNonDeterministicStateTwo implements IState
{
    public function on_up(){}
    public function on_down(){}
}

class TestNonDeterministicStateThree implements IState
{
    public function on_left(){}
    public function on_right(){}
}

class TestNonDeterministicStateFour implements IState
{
    public function on_again(){}
}
```

With this State Machine, the keys are States, and the transitions are listed as an value of the State.

Create a Runner and run:

```php
use IpelaMarshal\Handlers\Runner;

$process_definition = new NonDeterministic;
$action = "no";
$current_state = new TestNonDeterministicStateOne;

$res1 = Runner::run(
    $process_definition,
    $current_state,
    $action,
    $value
);

$current_state = new $res1;
$action = "right";

$res2 = Runner::run(
    $process_definition,
    $current_state,
    $value,
    $action,
);

echo $res1 //prints `'TestNonDeterministicStateThree'
echo $res2 //prints `'TestNonDeterministicStateTwo'
```

When there are no more states to run, the Runner will return false.

## Roadmap
- implement before and after lifecycle hooks
- visualise State Machine

## License

[MIT](https://choosealicense.com/licenses/mit/)
